# coding=utf-8
import os
import sys
import json
import uuid
import shutil
import hashlib
from io import BytesIO
from flask import Flask, request, redirect, jsonify, send_file, Response

app = Flask(__name__)
image_folder = sys.argv[1]
image_files = []
for root, dirs, files in os.walk(image_folder):
    for f in files:
        path = os.path.join(root, f)
        path_l = path.lower()
        if path_l.endswith('.png'):
            image_files.append(path)
        elif path_l.endswith('.jpg'):
            image_files.append(path)
image_files = sorted(image_files)
if not image_files:
    sys.exit(1)


@app.route('/image', methods=['GET'])
def image_get():
    path = request.args['path']
    return send_file(path)


@app.route('/path', methods=['POST'])
def path_post():
    idx = request.get_json().get('idx')
    if not isinstance(idx, int):
        return jsonify({'status': 'error', 'info': 'idx is not an integer'}), 400
    path = image_files[idx]
    exists = False
    print(f'JSON path: {path}')
    try:
        with open(f'{path}.json') as f:
            roi = json.load(f)['roi']
            if len(roi) > 0:
                exists = True
    except (json.JSONDecodeError, FileNotFoundError,):
        pass
    if idx < len(image_files):
        return jsonify({
            'path': path,
            'exists': exists
        }), 200
    else:
        return jsonify({'status': 'error', 'info': 'exceeded max idx'}), 400


@app.route('/annotation', methods=['POST'])
def annotation_post():
    j = request.get_json()
    roi = j.get('roi')
    idx = int(request.args['idx'])
    path = image_files[idx]
    # only save a json if 1+ annotations were created
    if isinstance(roi, list) and len(roi) > 0:
        fname = f'{path}.json'
        print(f'Saving annotation JSON to {fname}')
        with open(fname, 'w') as f:
            json.dump(j, f, sort_keys=True, indent=4)
    return jsonify(ok=True), 200


@app.route('/', methods=['GET'])
def root_get():
    return redirect('/static/draw.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
